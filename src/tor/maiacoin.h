/* Copyright (c) 2014, Paradime Developers */
/* See LICENSE for licensing information */

/**
 * \file Paradime.h
 * \brief Headers for Paradime.cpp
 **/

#ifndef TOR_Paradime_H
#define TOR_Paradime_H

#ifdef __cplusplus
extern "C" {
#endif

    char const* Paradime_tor_data_directory(
    );

    char const* Paradime_service_directory(
    );

    int check_interrupted(
    );

    void set_initialized(
    );

    void wait_initialized(
    );

#ifdef __cplusplus
}
#endif

#endif

